<?php

namespace EPAM\training\PHP\Sample\Calculator;

use PHPUnit\Framework\TestCase;

spl_autoload_register(function ($class) {

    $prefix = "EPAM\\training\\PHP\\Sample\\Calculator\\";
    $base_dir = __DIR__ . '/../../src/class/';

    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }

    $relative_class = substr($class, $len);
    $file = str_replace('\\', '/', $base_dir . $relative_class) . '.php';

    echo $file;

    if (file_exists($file)) {
        require $file;
    }
});

class CalcTest extends TestCase
{
    private static Calc $calc;

    public static function setUpBeforeClass(): void
    {
        self::$calc = new Calc(5);
    }

    public function testSum()
    {
        $this->assertEquals(3.76345, self::$calc->sum(1, 2.76345));
    }

    public function testSub()
    {
        $this->assertEquals(-1.76345, self::$calc->sub(1, 2.76345));
    }

    public function testMul()
    {
        $this->assertEquals(6, self::$calc->mul(2, 3));
    }

    public function testDiv()
    {
        $this->assertEquals(3.5, self::$calc->div(7, 2));
    }
}
